/* eslint-env node */
module.exports = {
  root: true,
  // 继承其他规则
  extends: [
    'plugin:vue/vue3-essential',
    'eslint:recommended',
  ],
  // 解析选项
  parserOptions: {
    ecmaVersion: 'latest', // ES语法
    sourceType: "module", // ES模块化
  },
  env:{
    browser: true,
    node:true,
    es6: true
  },
  plugins:[
      'vue'
  ]
}
