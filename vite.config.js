import { fileURLToPath, URL } from 'node:url'
import path from 'path'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import {ElementPlusResolver} from 'unplugin-vue-components/resolvers'
// https://vitejs.dev/config/
export default defineConfig({
  // vite proxy
  server:{
    proxy:{
      '/rabbit':{
        target:'http://pcapi-xiaotuxian-front-devtest.itheima.net/',
        // 是否端口欺骗
        changeOrigin:true,
        rewrite: (path) => path.replace(/^\/rabbit/, ''),
      }
    }
  },
  plugins: [
    vue(),
    AutoImport({
      resolvers:[ElementPlusResolver()],
    }),
    Components({
      resolvers:[ElementPlusResolver({importStyle:"sass"})],
    })
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  css:{
    preprocessorOptions:{
      // scss:{
      //   additionalData:`@use "@/styles/element/index.scss" as *;`,
      // },
      less:{
        charset:false,
        additionalData:[
          `@import "@/assets/styles/variables.less" ;
          @import "@/assets/styles/mixins.less";`
        ]
      }
    }
  },
  optimizeDeps:{
    exclude:['vue-demi'],
  },
})
