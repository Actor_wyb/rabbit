import request from "@/utils/request"

export const findRelGoods = (id, limit = 16) => {
    return request('/goods/relevant', 'GET', {id, limit})
}

export const findHotGoods = ({id, type, limit = 3}) => {
    return request('/goods/hot', 'GET', {id, type, limit})
}

export const findCommentInfoByGoods = (id) => {
    return request(`https://mock.boxuegu.com/mock/1175/goods/${id}/evaluate`, 'GET')
}

export const findCommentListByGoods = (id, reqParams) => {
  return request(`https://mock.boxuegu.com/mock/1175/goods/${id}/evaluate/page`, 'GET', reqParams)
}

export const getSpecsAndSkus = (skuId) => {
    return request(`/goods/sku/${skuId}`, 'GET')
}