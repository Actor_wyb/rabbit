import request from "@/utils/request";
// 获取结算信息

export const findCheckoutInfo = () => {
    return request('/member/order/pre', 'GET')
}

// 添加收获地址
export const addAddress = (address) => {
    return request('/member/address', 'POST', address)
}

// 编辑收货地址
export const editAddress = (address) => {
    return request('/member/address', 'PUT', address)
}

//提交订单
export const createOrder = (order) => {
    return request('/member/order', 'POST', order)
}

//获取订单详情
export const findOrder = (id) => {
    return request('/member/order/' + id, 'GET')
}

//获取订单列表
export const findOrderList = ({orderState, page, pageSize}) => {
    return request('/member/order', 'GET', {orderState,page,pageSize})
}


// 取消订单
export const cancelOrder = (orderId, cancelReason) => {
    return request(`/member/order/${orderId}/cancel`, 'PUT', {cancelReason})
}

//删除订单
export const deleteOrder = (ids) => {
    return request('/member/order', 'DELETE', {ids})
}

// 确认收货
export const confirmOrder = (id) => {
    return request(`/member/order/${id}/receipt`,'PUT' )
}

//查看物流
export const logisticsOrder = (id) => {
    return request(`/member/order/${id}/logistics`, 'GET')
}

export const findOrderRepurchase = (id) => {
    return request(`/member/order/repurchase/${id}`, 'GET')
}