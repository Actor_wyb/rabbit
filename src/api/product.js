import request from "@/utils/request";

// 获取商品详情
export const findGoods = (id) => {
    try {
        return request('/goods', 'GET', {id})
    } catch (error) {
        console.log("this is findGoods", error);
    }
}