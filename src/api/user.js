// import request from "@/utils/request";
import {useUserStore} from "@/store/modules/user";
// import {storeToRefs} from "pinia";

// 短信登录

// 获取短信验证码


// 账号密码登录
export const userAccountLogin = (account, password) => {
    const userStore = useUserStore()
    const {profile} = userStore
    const { permission } = userStore
    return new Promise((resolve, reject) => {
        if (profile.password === password && profile.account === account) {
            permission()
            resolve(true)
        }
        else reject(false)
    })
}