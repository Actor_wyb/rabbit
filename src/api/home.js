import request from "@/utils/request"

// limit参数请求为请求数据量
export const findBrand = (limit) =>{
    try{
        return request('/home/brand', 'GET', {limit})
    }catch(err){
        console.log("findBrand error", err);
    }
}

export const findBanner = ()=>{
    try{
        return request('/home/banner', 'GET')
    }catch(error){
        console.log("findBanner error", error);
    }
}

export const findNew = ()=>{
    try{
        return request('/home/new', 'GET')
    }catch(err){
        console.log("findNew error", err)
    }
}

export const findHot = ()=>{
    try{
        return request('/home/hot', 'GET')
    }catch(err){
        console.log("findHot error", err)
    }
}

export const findGoods = ()=>{
    try{
        return request('/home/goods', 'GET')
    }catch(err){
        console.log("findGoods error", err)
    }
}

export const findSpecial = ()=>{
    try{
        return request('/home/special', 'GET')
    }catch(err){
        console.log("findSpecial error", err)
    }
}