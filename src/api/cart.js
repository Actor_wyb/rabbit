import request from "@/utils/request";

export const getNewCartGoods = (skuId) => {
    return request(`/goods/stock/${skuId}`, 'GET')
}

export const MergeLocalCart = (cartList) => {
    return request('/member/cart/merge', 'POST', cartList)
}

export const findCartList_request = () => {
    return request('/member/cart', 'GET')
}

export const insertCart_request = ({skuId, count}) => {
    return request('/member/cart', 'POST', {skuId, count})
}

export const deleteCart_request = (ids) => {
    return request('/member/cart', 'DELETE', {ids})
}

export const updateCart_request = (goods) => {
    return request(`/member/cart/${goods.skuId}`, 'PUT', goods)
}

export const checkAllCart_request = ({selected, ids}) => {
    return request('/member/cart/selected', 'put', {selected, ids})
}