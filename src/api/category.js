import request from "@/utils/request";

// 向后端API请求获取首页Header的数据
export const findAllCategory = () => {
    // 不携带参数的GET请求
    try{
        return request('/home/category/head', 'GET')
    }catch(err){
        console.log("findAllCategory err", err);
    }
}

// 获取顶部分类数据
export const findTopCategory = (id) => {
    try{
        return request('/category', 'GET', {id})
    }catch(err){
        console.log("findTopCategory err", err);
    }
}

// 获取二级分类筛选条件数据
export const findSubCategoryFilter = (id) => {
    try{
        return request('/category/sub/filter', 'get', {id})
    }catch(err){
        console.log("findSubCategoryFilter err", err);
    }
}

// 获取分类下的商品
export const findSubCategoryGoods = (params) => {
    try{
        return request('/category/goods/temporary', 'POST', params)
    }catch(err) {
        console.log("findSubCategoryGoods err", err);
    }
}