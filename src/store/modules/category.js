import { ref } from "vue";
import { defineStore } from "pinia";
import { topCategory } from "@/api/constants";
import { findAllCategory } from "@/api/category";

// 分类模块
export const useCategoryStore = defineStore("category", () => {
    // item为数组元素，函数返回值返回给对应元素
    const list = ref(topCategory.map(item => ({ "name": item })))
    const getList = async ()=>{
            // 拿到Promise执行成功的结果response（是一个数据包含在data属性的Object对象）
            const {data:{result}} = await findAllCategory();
            result.forEach(item=>{
                item.open = false
            })
            list.value = result;
            console.log("this is category",result);
    }
    function show(item) {
        const category = list.value.find(category => category.id === item.id)
        category.open = true
    }

    function hide(item) {
        const category = list.value.find(category => category.id === item.id)
        category.open = false
    }

    return {
        // 分类信息list
        list,
        getList,
        show,
        hide,
    }
})

