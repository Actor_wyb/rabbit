import { reactive } from "vue";
import { defineStore } from "pinia";


// 用户信息模块
export const useUserStore = defineStore("user", ()=>{
    let profile = reactive({
        account:'wyb',
        phoneNumber:'19372436692',
        password:'123456',
        token:false,
        avatar:'../../assets/images/jinde.jpg'
    })
    // eslint-disable-next-line no-unused-vars
    const redirectUrl = '/'

    const permission = () => {
        profile.token = true
    }

    return {
        profile,
        permission
    }
})