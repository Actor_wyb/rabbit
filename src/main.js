import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import { createPinia } from "pinia";
import ui from './components/library/index'



import 'normalize.css'
import '@/assets/styles/common.less'
import './mock'

// 创建app应用实例
const app = createApp(App)
const pinia = createPinia()
// 创建状态管理库
app.use(pinia)

// 在根组件上挂载路由
app.use(router)

// 注册插件
app.use(ui)

// 在id为app的元素上挂载app
app.mount('#app')
