import XtxSkeleton from "./XtxSkeleton.vue";
import XtxCarousel from './XtxCarousel.vue'
import XtxMore from "./XtxMore.vue";
import XtxBread from "./XtxBread.vue";
import XtxCheckBox from "./XtxCheckBox.vue";
import XtxCity from "./XtxCity.vue";
import XtxNumBox from "./XtxNumBox.vue";
import XtxInfiniteLoading from './XtxInfiniteLoading.vue';
import XtxButton from './XtxButton.vue';
import XtxPagination from "./XtxPagination.vue";
import XtxMessage from "./XtxMessage.vue";
import XtxConfirm from "./XtxConfirm.vue";
import XtxDialog from "@/components/library/XtxDialog.vue";
import XtxTabs from "@/components/library/XtxTabs.vue";
import XtxTabsPanel from "@/components/library/XtxTabsPanel.vue";
import XtxSteps from "@/components/library/XtxSteps.vue";
import XtxStepsItem from "@/components/library/XtxStepsItem.vue";
import Confirm from './confirm'
import message from "./message";

/* require.context(directory,useSubdirectories,regExp) webpack提供的自动导入API
directory:要搜索的目录路径
useSubdirectories：是否检查目录下的子文件夹
regExp：正则表达式匹配文件名
搜索目录下的.vue文件*/
// const importFn = require.context("./", false, /\.vue$/)

const ui = {
    // 全局注册Xtx组件，不用显式引入即可使用组件
    install(app) {
        app.component(XtxSkeleton.name, XtxSkeleton);
        app.component(XtxCarousel.name, XtxCarousel);
        app.component(XtxMore.name, XtxMore);
        app.component(XtxBread.name, XtxBread);
        app.component(XtxCheckBox.name, XtxCheckBox);
        app.component(XtxCity.name, XtxCity);
        app.component(XtxNumBox.name, XtxNumBox);
        app.component(XtxInfiniteLoading.name, XtxInfiniteLoading);
        app.component(XtxButton.name, XtxButton);
        app.component(XtxPagination.name, XtxPagination);
        app.component(XtxMessage.name, XtxMessage)
        app.component('XtxConfirm', XtxConfirm)
        app.component('XtxDialog', XtxDialog)
        app.component(XtxTabsPanel.name, XtxTabsPanel)
        app.component(XtxTabs.name, XtxTabs)
        app.component(XtxSteps.name, XtxSteps)
        app.component(XtxStepsItem.name, XtxStepsItem)
        // 封装图片懒加载指令
        defineDirective(app);
        app.config.globalProperties.$message = message; // 原型
        app.config.globalProperties.$confirm = Confirm;
    }
}
export default ui;

const defineDirective = (app)=>{{
    // v-lazy-img
    app.directive('lazy-img', {
        // el：绑定DOM元素，binding：DOM元素属性值
        mounted(el, binding) {
            const observer = new IntersectionObserver(
                ([{ isIntersecting }])=>{
                    if(isIntersecting){
                        observer.unobserve(el)
                        el.src = binding.value
                    }
                },
                {
                    // DOM元素进入视口0.01，执行懒加载并解绑监听
                    threshold: 0.01
                })
            observer.observe(el)
        }
    })
}}