import { createVNode, render } from 'vue';
import XtxMessage from './XtxMessage.vue';

const div = document.createElement('div')
// 设置div元素的属性和值
div.setAttribute('class', 'xtx-message-container')
// 把div节点追加给body
document.body.appendChild(div)

// 定时器标识符
let timer = null

export default ({type, text}) => {
    // 根据xtxmessage组件创建虚拟节点
    const vnode = createVNode(XtxMessage,{type, text})

    // vnode渲染到dom容器中
    render(vnode, div)
    clearTimeout(timer)
    timer = setTimeout(()=> {
        render(null, div)
    }, 3000)
}