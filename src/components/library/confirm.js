import { createVNode, render } from "vue";
import XtxConfirm from "@/components/library/XtxConfirm.vue";

const div = document.createElement('div')
div.setAttribute('class', 'xtx-confirm-container')
document.body.appendChild(div)

export default ({title, text}) => {
    return new Promise((resolve, reject)=> {
        const submitCallback = () => {
            render(null, div)
            reject()
        }

        const cancelCallback = () => {
            render(null, div)
            reject(new Error('点击取消'))
        }

        // params(type:string|component, props:object|null, children:children|slot|slots)
        const vnode = createVNode(XtxConfirm, {title, text, submitCallback, cancelCallback})
        // 把创建的虚拟节点渲染到div中
        render(vnode, div)
    })
}