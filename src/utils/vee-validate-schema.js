// 定义校验规则提供给vee-validate组件
// 用于用户验证
export default {
    // 校验account,这是账号密码登录
    account(value) {
        // 表单值为空
        if(!value) return '请输入用户名'
        if(!/^[a-zA-Z]\w{2,19}$/.test(value)) return '字母开头且3-20个字符'

        return true
    },
    password(value) {
        if(!value) return '请输入密码'
        if (!/^\w{6,24}$/.test(value)) return '密码是6-24个字符'

        return true
    },

    // 这是短信验证登录 
    phoneNumber(value) {
        if(!value) return '请输入手机号'
        if(!/^1[3-9]\d{9}$/.test(value)) return '手机号格式不正确'

        return true
    },

    code(value) {
        if(!value) return '请输入验证码'
        if(!/^\d{6}$/.test(value)) return '验证码是6位数字'

        return true
    }
}