import axios from "axios";
// import router from "@/router";
// import { useUserStore } from "@/store/modules/user";
// import { storeToRefs } from "pinia";

// const userStore = useUserStore()

// 解构赋值user.js中的setUser方法
// const {setUser} = userStore

// 用于向该地址发送网络请求
export const baseURL = 'http://pcapi-xiaotuxian-front-devtest.itheima.net/'
const instance = axios.create({
    baseURL,
    timeout: 5000
});

// instance.interceptors.request.use(config => {
//     // 若请求成功,则导入用户信息
//     const {profile} = storeToRefs(userStore)
//     // 判断是否有token
//     if (profile.token) {
//         // 在请求头中附加token
//         config.headers.Authorization = `Bearer ${profile.token}`
//     }
//     return config
// }, error => {
//     return Promise.reject(error)
// })

// instance.interceptors.response.use(res=>{
//     return res.data
// },error=>{
//     // 用户未登录，响应码为401，需要身份验证，应该回到登录页
//     if(error.response && error.response.status === 401) {
//         // 响应失败，所以清空用户信息
//         setUser({})

//         // router.currentRoute.value.fullPath为当前路由地址,router.currentRoute是ref响应式
//         const fullPath = encodeURIComponent(router.currentRoute.value.fullPath)
//         // ebcodeURIComponent转换uri编码，防止解析地址出问题
//         router.push('/login?redirectUrl=' + fullPath)
//     }
//     return Promise.reject(error)
// })


// 请求工具函数，里面封装了axios请求,
export default (url, method, submitData)=>{
    return instance({
        url,
        method,
        // toLowerCase()方法转换成小写再进行判断
        [method.toLowerCase() === 'get' ? 'params' : 'data']:submitData
    })
}