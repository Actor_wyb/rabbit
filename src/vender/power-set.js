// 计算所给集合的幂集

// originalSet是原始的sku数据
// 导出的箭头函数命名PowerSet
export default (originalSet) => {
    const subSets = [];

    // 计算可能的组合数（2^n，其中 n 是原始集合的长度,  组合数中包括空集）
    const numberOfCombinations = 2 ** originalSet.length;

    // 遍历所有可能的组合。
    for(let combinationIndex = 0; combinationIndex < numberOfCombinations; combinationIndex++) {
        const subSet = [];

        // 遍历原始集合的每个元素
        for(let setElementIndex = 0; setElementIndex < originalSet.length; setElementIndex++) {
            // 根据二进制表示法判断当前元素是否应包含在子集中
            // 1左移setElementIndex位
            if(combinationIndex & (1 << setElementIndex)) {
                subSet.push(originalSet[setElementIndex]);
            }
        }

        // 当前子集添加到所有子集的列表中
        subSets.push(subSet);
    }

    return subSets;
}