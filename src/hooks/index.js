import { useIntersectionObserver, useIntervalFn } from "@vueuse/core";
import {ref, onUnmounted} from 'vue';
import dayjs from 'dayjs';

export const lazyLoadingData = (apiFn) => {
    const target = ref(null)
    const {stop} = useIntersectionObserver(
        target,
        ([{isIntersecting}]) => {
            if(isIntersecting) {
                apiFn()
                stop()
            }
        },
    )
    return {
        target,
    }
}

export const usePayTime = () => {
//     倒计时
    const time = ref(0)
    const timeText = ref('')
    const { pause, resume } = useIntervalFn(()=>{
        time.value--
        timeText.value = dayjs.unix(time.value).format('mm分ss秒')
        if(time.value <= 0) {
            pause()
        }
    }, 1000, false)
    onUnmounted(()=>{
        // 暂停间隔
        pause()
    })

//     开启定时器countdown倒计时时间
    const start = (countdown) => {
        time.value = countdown
        timeText.value = dayjs.unix(time.value).format('mm分ss秒')
        // 恢复间隔
        resume()
    }

    return {
        start,
        timeText
    }
}